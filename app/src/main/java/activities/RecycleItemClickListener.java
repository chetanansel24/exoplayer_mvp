package activities;

import java.util.ArrayList;

import model.ImageData;

public interface RecycleItemClickListener {

    /**
     * @param position
     * @param imageData
     */
    void onClick(int position, ArrayList<ImageData> imageData);

}
