package activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.mobiotics.demo.R;

import java.util.ArrayList;

import fragment.ImageFragment;
import fragment.VideoFragment;
import model.ImageData;

public class MainActivity extends AppCompatActivity implements RecycleItemClickListener {

    private static final String POSITION = "POSITION";
    private static final String LIST = "LIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFragment(ImageFragment.newInstance(), false);
    }

    private void getFragment(@NonNull Fragment fragment, final boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(int position, ArrayList<ImageData> imageData) {
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        bundle.putSerializable(LIST, imageData);
        VideoFragment videoFragment = VideoFragment.newInstance(bundle);
        getFragment(videoFragment, true);
    }
}
