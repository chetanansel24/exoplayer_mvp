package model;

import java.io.Serializable;

/**
 *
 */
public class ImageData implements Serializable {

    private String title;
    private String description;
    private String thumb;
    private String url;
    private String id;

    public ImageData(String thumb, String title, String description, String url) {
        this.title = title;
        this.description = description;
        this.thumb = thumb;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getThumb() {
        return thumb;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    public ImageData() {

    }
}
