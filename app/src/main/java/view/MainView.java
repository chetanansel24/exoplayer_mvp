package view;

import java.util.ArrayList;

import model.ImageData;

public interface MainView {

    void showProgressLoader();

    void hideProgressLoader();

    public void onSuccess(ArrayList<ImageData> imageData);

    public void onError(String message);
}
