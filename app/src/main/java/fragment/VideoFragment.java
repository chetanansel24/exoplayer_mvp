package fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.exoplayer2.ui.PlayerView;
import com.mobiotics.demo.R;

import java.util.ArrayList;

import adapter.ImageListAdapter;
import adapter.RecyclerViewClickListener;
import model.ImageData;
import presenter.VideoPlayPresenterImpl;
import view.ExoPlayerView;

public class VideoFragment extends Fragment implements RecyclerViewClickListener, ExoPlayerView {

    private static final String POSITION = "POSITION";
    private static final String LIST = "LIST";
    private ArrayList<ImageData> imageData = new ArrayList<>();
    private PlayerView simpleExoPlayerView;
    public TextView author;
    private int itemClickPosition = -1;
    private RecyclerView recyclerView;
    private ImageListAdapter imageListAdapter;
    private VideoPlayPresenterImpl videoPlayPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemClickPosition = getArguments().getInt(POSITION);
            imageData = (ArrayList<ImageData>) getArguments().getSerializable(LIST);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        removeItem(itemClickPosition);
        simpleExoPlayerView = view.findViewById(R.id.player);
        author = view.findViewById(R.id.author);
        author.setText(imageData.get(itemClickPosition).getTitle());
        videoPlayPresenter = new VideoPlayPresenterImpl(this);
        initVideoView(imageData.get(itemClickPosition).getUrl(), simpleExoPlayerView);
        return view;
    }

    @Override
    public void onClick(int position) {
        itemClickPosition = position;
        author.setText(imageData.get(itemClickPosition).getTitle());
        initVideoView(imageData.get(itemClickPosition).getUrl(), simpleExoPlayerView);
        removeItem(itemClickPosition);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        videoPlayPresenter.releaseExoPlayer();
    }


    @Override
    public void onPlayerStateChanged() {
        int newVideoPosition = itemClickPosition + 1;
        if (newVideoPosition == imageData.size()) {
            newVideoPosition = 0;
        }
        author.setText(imageData.get(newVideoPosition).getTitle());
        initVideoView(imageData.get(newVideoPosition).getUrl(), simpleExoPlayerView);
        removeItem(newVideoPosition);
    }

    private void initVideoView(String url, final PlayerView playerView) {
        Uri uri = Uri.parse(url);
        playerView.setPlayer(videoPlayPresenter.getExoPlayer(getContext()));
        videoPlayPresenter.playVideo(uri);
        videoPlayPresenter.addListener();
    }

    private void removeItem(int i) {
        imageListAdapter = new ImageListAdapter(this, getContext(), imageData, i);
        recyclerView.setAdapter(imageListAdapter);
        imageListAdapter.notifyDataSetChanged();
    }

    public static VideoFragment newInstance(Bundle bundle) {
        VideoFragment videoFragment = new VideoFragment();
        videoFragment.setArguments(bundle);
        return videoFragment;
    }
}