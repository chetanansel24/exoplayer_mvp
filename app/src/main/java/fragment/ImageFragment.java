package fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobiotics.demo.R;

import java.util.ArrayList;

import activities.RecycleItemClickListener;
import adapter.ImageListAdapter;
import adapter.RecyclerViewClickListener;
import model.ImageData;
import presenter.MainPresenterImpl;
import view.MainView;

public class ImageFragment extends Fragment implements MainView {

    private RecyclerView recyclerView;
    private View mLoadingView;
    private MainPresenterImpl mainPresenter;
    private TextView mTextError;
    private RecycleItemClickListener myListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.imagelist, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        mLoadingView = view.findViewById(R.id.loading_view);
        mTextError = view.findViewById(R.id.error_view);
        mainPresenter = new MainPresenterImpl(this);
        mainPresenter.getData();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        myListener = (RecycleItemClickListener) context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainPresenter.onDestroy();
    }

    @Override
    public void showProgressLoader() {
        recyclerView.setVisibility(View.GONE);
        mTextError.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressLoader() {
        recyclerView.setVisibility(View.VISIBLE);
        mTextError.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(final ArrayList<ImageData> imageData) {
        ImageListAdapter imageListAdapter = new ImageListAdapter(new RecyclerViewClickListener() {
            @Override
            public void onClick(int position) {
                myListener.onClick(position, imageData);
            }
        }, getContext(), imageData);
        recyclerView.setAdapter(imageListAdapter);
    }

    @Override
    public void onError(String message) {
        mTextError.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
    }

    /**
     * @return ImageFragment
     */
    public static ImageFragment newInstance() {
        return new ImageFragment();
    }
}