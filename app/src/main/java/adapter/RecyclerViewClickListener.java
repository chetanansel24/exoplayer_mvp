package adapter;

public interface RecyclerViewClickListener {
    void onClick(int position);
}
