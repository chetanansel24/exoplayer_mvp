package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mobiotics.demo.R;

import java.util.ArrayList;
import java.util.List;

import model.ImageData;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder> {

    private List<ImageData> imageAndTextList;
    private Context mContext;
    private String TAG = "Database";
    private RecyclerViewClickListener recyclerViewClickListener;
    private int positionToHide = -1;

    public ImageListAdapter(RecyclerViewClickListener recyclerViewClickListener, Context mContext,
                            List<ImageData> imageAndTextList) {
        this.imageAndTextList = imageAndTextList;
        this.mContext = mContext;
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    public ImageListAdapter(RecyclerViewClickListener recyclerViewClickListener, Context mContext,
                            List<ImageData> imageAndTextList, int position) {
        this.imageAndTextList = imageAndTextList;
        this.mContext = mContext;
        this.recyclerViewClickListener = recyclerViewClickListener;
        positionToHide = position;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_list_row, parent, false);

        return new ImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_launcher);
        Glide.with(mContext)
                .setDefaultRequestOptions(requestOptions)
                .load(imageAndTextList.get(position).getThumb())
                .into(holder.image);

        holder.description.setText(imageAndTextList.get(position).getDescription());
        holder.title.setText(imageAndTextList.get(position).getTitle());

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewClickListener.onClick(position);
            }
        });

        if (positionToHide == position) {
            holder.image.setVisibility(View.GONE);
            holder.description.setVisibility(View.GONE);
            holder.title.setVisibility(View.GONE);
        } else {
            holder.image.setVisibility(View.VISIBLE);
            holder.description.setVisibility(View.VISIBLE);
            holder.title.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public int getItemCount() {
        return imageAndTextList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title, description;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
        }
    }
}
