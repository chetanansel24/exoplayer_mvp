package presenter;

import android.app.Activity;
import android.content.Context;

import com.firebase.ui.auth.AuthUI;

import java.util.Arrays;
import java.util.List;

public class FireBaseLoginPresenterImpl {

    private static final int RC_SIGN_IN = 101;

    public void initLogin(Activity activity){
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.GoogleBuilder().build());
        activity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }
}
