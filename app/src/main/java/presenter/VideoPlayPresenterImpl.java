package presenter;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.ExoPlayer;

import view.ExoPlayerView;
import model.ExoVideoPlayer;

public class VideoPlayPresenterImpl implements PlayVideoPresenter {

    ExoVideoPlayer exoVideoPlayer = new ExoVideoPlayer();
    ExoPlayerView exoPlayer;

    public VideoPlayPresenterImpl(ExoPlayerView exoPlayer) {
        this.exoPlayer = exoPlayer;

    }


    @Override
    public void addListener() {
        exoVideoPlayer.initEvent(exoPlayer);
    }

    @Override
    public ExoPlayer getExoPlayer(Context context) {
        return exoVideoPlayer.getExoPLayer(context);
    }

    @Override
    public void releaseExoPlayer() {
        exoVideoPlayer.release();
    }

    @Override
    public void playVideo(Uri uri) {
        exoVideoPlayer.playVideo(uri);
    }

    @Override
    public long getVideoSeekTo() {
        return exoVideoPlayer.getSeekTime();
    }

    @Override
    public void setVideoSeekTo(long seekTime) {
        exoVideoPlayer.setSeekTime(seekTime);
    }

}
