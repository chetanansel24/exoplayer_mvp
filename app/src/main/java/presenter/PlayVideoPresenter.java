package presenter;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.ExoPlayer;

public interface PlayVideoPresenter {

    public void addListener();

    public ExoPlayer getExoPlayer(Context context);

    public void releaseExoPlayer();

    public void playVideo(Uri uri);

    public long getVideoSeekTo();

    public void setVideoSeekTo(long seekTo);
}
