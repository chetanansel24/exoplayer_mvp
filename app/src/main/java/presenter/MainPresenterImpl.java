package presenter;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import model.ImageData;
import view.MainView;

public class MainPresenterImpl implements FireBasePresenter {

    private static final String TAG = MainPresenterImpl.class.getSimpleName();
    private MainView mainView;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void getData() {
        mainView.showProgressLoader();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<ImageData>> genericTypeIndicator = new
                        GenericTypeIndicator<ArrayList<ImageData>>() {
                        };
                mainView.hideProgressLoader();
                mainView.onSuccess(dataSnapshot.getValue(genericTypeIndicator));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "DatabaseError", error.toException());
                mainView.hideProgressLoader();
                mainView.onError("Something went Wrong, Please try Again");
            }
        });

    }

    @Override
    public void onDestroy() {
        mainView = null;
    }
}