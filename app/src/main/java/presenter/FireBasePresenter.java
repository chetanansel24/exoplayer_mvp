package presenter;

/**
 *
 */
public interface FireBasePresenter {

    /**
     * get firebase data
     */
    public void getData();

    /**
     *  destroy view
     */
    void onDestroy();
}
